<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 07/03/2021
 * Time: 11:00
 */

namespace App\abService\PaymentBundle\Services;

use App\Entity\Transaction;
use App\Repository\WebsiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;

interface WaafiPayServiceInterface extends PaymentInterface
{
}