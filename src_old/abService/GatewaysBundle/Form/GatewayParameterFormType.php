<?php


namespace  App\abService\GatewaysBundle\Form;

use App\Entity\GatewayParameter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GatewayParameterFormType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),
                'label'=>"Parameter name"

            ))
            ->add('value', TextType::class,array(
                'required' => false,
                'attr'=>array('class'=>"form-control"),
                'label'=>"Parameter value"

            ))
            ->add('isRequired', CheckboxType::class,array(
                'attr'=>array('class'=>"form-control"),
                'required'=>false,
                'label'=>"Parameter is required"
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GatewayParameter::class,
        ]);
    }
}