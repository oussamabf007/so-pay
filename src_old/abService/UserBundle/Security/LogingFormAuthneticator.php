<?php


namespace App\abService\UserBundle\Security;

use App\abService\UserBundle\Controller\LoginController;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;

class LogingFormAuthneticator extends AbstractAuthenticator
{

    private $userRepository;
    private $urlGenerator;
    private $em;

    /**
     * LogingFormAuthneticator constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $em
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function  __construct(userRepository $userRepository, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator)
    {
        $this->userRepository = $userRepository;
        $this->urlGenerator = $urlGenerator;
        $this->em = $em;
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * Returning null means authenticate() can be called lazily when accessing the token storage.
     */
    public function supports(Request $request): ?bool{

        //get requested route name.
        $currentRouteName = $request->attributes->get('_route');

        if($currentRouteName === "user_login" && $request->isMethod("POST"))
            return true;
        else
            return false;

    }

    /**
     * Create a passport for the current request.
     *
     * The passport contains the user, credentials and any additional information
     * that has to be checked by the Symfony Security system. For example, a login
     * form authenticator will probably return a passport containing the user, the
     * presented password and the CSRF token value.
     *
     * You may throw any AuthenticationException in this method in case of error (e.g.
     * a UsernameNotFoundException when the user cannot be found).
     *
     * @throws AuthenticationException
     */
    public function authenticate(Request $request): PassportInterface{

        //Get requested email address.
        $email = $request->request->get("email");

        //save the requested email to rewrite it to user.
        $request->getSession()->set(LoginController::LAST_EMAIL, $email);

        //Get user by email address.
        $user = $this->userRepository->findOneByEmail($email);

        //If user not exist.
        if(!$user){
            throw new CustomUserMessageAuthenticationException('Invalid credentials');
        }

        //Return passport object.
        return new Passport(
            $user,
            new PasswordCredentials($request->request->get("password")),
            array(
                new CsrfTokenBadge("user_login_form",$request->request->get("csrf_token")),
                new RememberMeBadge
            )
        );
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): Response
    {

        //Delete user last email from session
        $request->getSession()->remove(LoginController::LAST_EMAIL);

        //Get current User.
        $currentUser = $token->getUser();

        //Set lastLogin datetime.
        $currentUser->setLastLogin(new \DateTime("now"));
        //Save changes.
        $this->em->persist($currentUser);
        $this->em->flush();

        return new RedirectResponse($this->urlGenerator->generate('dashboard_dashboard_index'));
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response{

        $request->getSession()->getFlashBag()->add("danger","Invalid credentials");

        return new RedirectResponse($this->urlGenerator->generate("user_login"));

    }
}