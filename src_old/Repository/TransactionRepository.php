<?php

namespace App\Repository;

use App\abService\ProjectBaseBundle\Repository\baseRepository;
use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends baseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    /**
     * @param $user
     * @return mixed
     */
    public function findAllByUser($user){
        //queryBuilder.
        $queryBuilder = $this->createQueryBuilder('t')
            ->leftJoin('t.website', 'tw')
            ->leftJoin('tw.users', 'twu')
            ->where('twu = :user')
            ->setParameter('user',$user);

        return $queryBuilder->getQuery()->getResult();
    }


    /**
     * @param null $user
     * @param null $gatewaysToken
     * @param null $websitesToken
     * @return mixed
     */
    public function getTransactionByGateways($user = null, $gatewaysToken = null, $websitesToken = null)
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->select("count(t) as total_per_gateway, g.color, g.name")
            ->leftJoin('t.gateway', "g")
            ->groupBy('t.gateway');
        if (!is_null($user)) {
            $queryBuilder
                ->leftJoin('t.website', 'tw')
                ->leftJoin('tw.users', 'twu')
                ->where('twu = :user')
                ->setParameter('user', $user);
        }

        //Filter by selected gateways.
        if(!is_null($gatewaysToken)){
            $queryBuilder
                ->andWhere("g.token IN  (:tokens)")
                ->setParameter('tokens', $gatewaysToken);
        }

        //Filter by selected websites.
        if(!is_null($websitesToken)){
            if(is_null($user))
                $queryBuilder
                    ->leftJoin('t.website', 'tw');

            $queryBuilder
                ->andWhere("tw.token IN  (:tw_tokens)")
                ->setParameter('tw_tokens', $websitesToken);
        }

        return $queryBuilder
            ->getQuery()
            ->getResult();
    }

    /**
     * @param null $user
     * @param null $gatewaysToken
     * @param null $websitesToken
     * @return mixed
     */
    public function getTransactionsStatusPerDay($user = null, $gatewaysToken = null, $websitesToken = null){
        $queryBuilder = $this->createQueryBuilder('t')
            ->select("count(t) as total_per_status, DATE(t.createdAt) as day, t.status")
            ->groupBy('day, t.status');
        if(!is_null($user)){
            $queryBuilder
                ->leftJoin('t.website', 'tw')
                ->leftJoin('tw.users', 'twu')
                ->where('twu = :user')
                ->setParameter('user',$user);
        }

        //Filter by selected gateways.
        if(!is_null($gatewaysToken)){
            $queryBuilder
                ->leftJoin('t.gateway', "g")
                ->andWhere("g.token IN  (:tokens)")
                ->setParameter('tokens', $gatewaysToken);
        }

        //Filter by selected websites.
        if(!is_null($websitesToken)){
            if(is_null($user))
                $queryBuilder
                    ->leftJoin('t.website', 'tw');

            $queryBuilder
                ->andWhere("tw.token IN  (:tw_tokens)")
                ->setParameter('tw_tokens', $websitesToken);
        }

        return $queryBuilder
            ->getQuery()
            ->getResult();

    }
    // /**
    //  * @return Transaction[] Returns an array of Transaction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transaction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
