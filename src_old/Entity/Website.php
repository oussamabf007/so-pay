<?php

namespace App\Entity;

use App\abService\ProjectBaseBundle\Entity\TimestampableTrait;
use App\Repository\WebsiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WebsiteRepository::class)
 * @ORM\Table(name="websites")
 * @ORM\HasLifecycleCallbacks
 */
class Website
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $ipAddress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $callbackUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secretKey;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hashKey;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="websites")
     * @ORM\JoinTable(
     *     name="users_websites",
     *     joinColumns={
     *         @ORM\JoinColumn(
     *             name="website_id",
     *             referencedColumnName="id",
     *             nullable=false
     *         )
     *     },
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)}
     * )
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=WebsiteGatewayConfiguration::class, mappedBy="website", cascade={"persist"}, orphanRemoval=true)
     */
    private $gatewaysConfigurations;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="website")
     */
    private $transactions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * Website constructor.
     */
    public function __construct()
    {
        //Set  isActive to true.
        $this->isActivated = true;
        $this->users = new ArrayCollection();
        //Generate hash and secretKey.
        $this->generateSecretKey();
        $this->generateHashKey();
        $this->gatewaysConfigurations = new ArrayCollection();
        $this->transactions = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getCallbackUrl(): ?string
    {
        return $this->callbackUrl;
    }

    public function setCallbackUrl(string $callbackUrl): self
    {
        $this->callbackUrl = $callbackUrl;

        return $this;
    }

    public function getSecretKey(): ?string
    {
        return $this->secretKey;
    }

    public function setSecretKey(string $secretKey): self
    {
        $this->secretKey = $secretKey;

        return $this;
    }

    public function getHashKey(): ?string
    {
        return $this->hashKey;
    }

    public function setHashKey(string $hashKey): self
    {
        $this->hashKey = $hashKey;

        return $this;
    }


    /**
     * @return ArrayCollection| User
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
        return $this;
    }

    /**
     *  Generate random hashKey.
     * @param int $length
     */
    public function generateHashKey($length = 10){
        $hashKey = $this->generateRandomString($length);
        $this->setHashKey($hashKey);
    }

    /**
     *  Generate random SecretKey.
     * @param int $length
     */
    public function generateSecretKey($length = 30){
        $secretKey = $this->generateRandomString($length);
        $this->setSecretKey($secretKey);
    }

    /**
     * @return Collection|WebsiteGatewayConfiguration[]
     */
    public function getGatewaysConfigurations(): Collection
    {
        return $this->gatewaysConfigurations;
    }

    public function addGatewaysConfiguration(WebsiteGatewayConfiguration $gatewaysConfiguration): self
    {
        if (!$this->gatewaysConfigurations->contains($gatewaysConfiguration)) {
            $this->gatewaysConfigurations[] = $gatewaysConfiguration;
            $gatewaysConfiguration->setWebsite($this);
        }

        return $this;
    }

    public function removeGatewaysConfiguration(WebsiteGatewayConfiguration $gatewaysConfiguration): self
    {
        if ($this->gatewaysConfigurations->contains($gatewaysConfiguration)) {
            $this->gatewaysConfigurations->removeElement($gatewaysConfiguration);
            // set the owning side to null (unless already changed)
            if ($gatewaysConfiguration->getWebsite() === $this) {
                $gatewaysConfiguration->setWebsite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setWebsite($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getWebsite() === $this) {
                $transaction->setWebsite(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
