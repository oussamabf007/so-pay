<?php

namespace App\Entity;

use App\abService\ProjectBaseBundle\Entity\TimestampableTrait;
use App\Repository\WebsiteGatewayConfigurationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WebsiteGatewayConfigurationRepository::class)
 * @ORM\Table(name="website_gateway_parameters")
 * @ORM\HasLifecycleCallbacks
 */
class WebsiteGatewayConfiguration
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Website::class, inversedBy="gatewaysConfigurations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $website;

    /**
     * @ORM\ManyToOne(targetEntity=Gateway::class, inversedBy="websiteGatewayConfigurations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $gateway;

    /**
     * @ORM\OneToMany(targetEntity=GatewayParameter::class, mappedBy="websiteGatewayConfiguration", cascade={"persist"})
     */
    private $gatewaysConfigurations;

    public function __construct()
    {
        $this->gatewaysConfigurations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(?Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return Collection|GatewayParameter[]
     */
    public function getGatewaysConfigurations(): Collection
    {
        return $this->gatewaysConfigurations;
    }

    public function addGatewaysConfiguration(GatewayParameter $gatewaysConfiguration): self
    {
        if (!$this->gatewaysConfigurations->contains($gatewaysConfiguration)) {
            $this->gatewaysConfigurations[] = $gatewaysConfiguration;
            $gatewaysConfiguration->setWebsiteGatewayConfiguration($this);
        }

        return $this;
    }

    public function removeGatewaysConfiguration(GatewayParameter $gatewaysConfiguration): self
    {
        if ($this->gatewaysConfigurations->contains($gatewaysConfiguration)) {
            $this->gatewaysConfigurations->removeElement($gatewaysConfiguration);
            // set the owning side to null (unless already changed)
            if ($gatewaysConfiguration->getWebsiteGatewayConfiguration() === $this) {
                $gatewaysConfiguration->setWebsiteGatewayConfiguration(null);
            }
        }

        return $this;
    }

    /**
     * @return Gateway|null
     */
    public function getGateway(): ?Gateway
    {
        return $this->gateway;
    }

    /**
     * @param Gateway|null $gateway
     * @return WebsiteGatewayConfiguration
     */
    public function setGateway(?Gateway $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Return this gatewayConfiguration base gatewayConfiguration.
     *
     * @return array|ArrayCollection
     */
    public function getGatewaysConfigurationsBases(){
        $result = new ArrayCollection();
        foreach ($this->getGatewaysConfigurations() as $gatewaysConfiguration)
            $result[] = $gatewaysConfiguration->getBaseGatewayParameter();

        return $result;
    }
}
