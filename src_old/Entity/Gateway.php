<?php

namespace App\Entity;

use App\abService\ProjectBaseBundle\Entity\TimestampableTrait;
use App\Repository\GatewayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GatewayRepository::class)
 * @ORM\Table(name="gateways")
 * @ORM\HasLifecycleCallbacks
 */
class Gateway
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $serviceId;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $extraFields = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=GatewayParameter::class, mappedBy="gateway", cascade={"persist"})
     */
    private $gatewayParameters;

    /**
     * @ORM\OneToMany(targetEntity=WebsiteGatewayConfiguration::class, mappedBy="gateway", cascade={"persist"})
     */
    private $websiteGatewayConfigurations;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="gateway")
     */
    private $transactions;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $color;

    /**
     * Gateway constructor.
     */
    public function __construct()
    {
        $this->isEnabled = true;

        //Extra fields Model.
        $extraFields = array(
            array(
                "id"=>"name of the input1 (label)",
                "type"=>"text|email|password",
                "label"=>"input label",
                'name'=>"input name",
                'placeholder'=>"input name",
            )
        );
        $this->setExtraFields($extraFields);
        $this->gatewayParameters = new ArrayCollection();
        $this->websiteGatewayConfigurations = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getServiceId(): ?string
    {
        return $this->serviceId;
    }

    public function setServiceId(string $ServiceId): self
    {
        $this->serviceId = $ServiceId;

        return $this;
    }

    /**
     * @return array
     */
    public function getExtraFields()
    {
        if(!is_null($this->extraFields))
            return json_encode($this->extraFields);
    }

    /**
     * @param $extraFields
     * @return Gateway
     */
    public function setExtraFields($extraFields): self
    {
        if (!is_array($extraFields))
            $extraFields = json_decode($extraFields);

        $this->extraFields = $extraFields;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|GatewayParameter[]
     */
    public function getGatewayParameters(): Collection
    {
        return $this->gatewayParameters;
    }

    public function addGatewayParameter(GatewayParameter $gatewayParameter): self
    {
        if (!$this->gatewayParameters->contains($gatewayParameter)) {
            $this->gatewayParameters[] = $gatewayParameter;
            $gatewayParameter->setGateway($this);
        }

        return $this;
    }

    public function removeGatewayParameter(GatewayParameter $gatewayParameter): self
    {
        if ($this->gatewayParameters->contains($gatewayParameter)) {
            $this->gatewayParameters->removeElement($gatewayParameter);
            // set the owning side to null (unless already changed)
            if ($gatewayParameter->getGateway() === $this) {
                $gatewayParameter->setGateway(null);
            }
        }

        return $this;
    }

    /**
     * Delete AllGatewayParameters.
     */
    public function removeAllGatewayParameters(){
        foreach ($this->gatewayParameters as $gatewayParameter){
            $this->removeGatewayParameter($gatewayParameter);
        }
    }

    /**
     * @return Collection|WebsiteGatewayConfiguration[]
     */
    public function getWebsiteGatewayConfigurations(): Collection
    {
        return $this->websiteGatewayConfigurations;
    }

    public function addWebsiteGatewayConfiguration(WebsiteGatewayConfiguration $websiteGatewayConfiguration): self
    {
        if (!$this->websiteGatewayConfigurations->contains($websiteGatewayConfiguration)) {
            $this->websiteGatewayConfigurations[] = $websiteGatewayConfiguration;
            $websiteGatewayConfiguration->setGateway($this);
        }

        return $this;
    }

    public function removeWebsiteGatewayConfiguration(WebsiteGatewayConfiguration $websiteGatewayConfiguration): self
    {
        if ($this->websiteGatewayConfigurations->contains($websiteGatewayConfiguration)) {
            $this->websiteGatewayConfigurations->removeElement($websiteGatewayConfiguration);
            // set the owning side to null (unless already changed)
            if ($websiteGatewayConfiguration->getGateway() === $this) {
                $websiteGatewayConfiguration->setGateway(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setGateway($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getGateway() === $this) {
                $transaction->setGateway(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
