<?php

namespace App\Entity;

use App\abService\ProjectBaseBundle\Entity\TimestampableTrait;
use App\Repository\GatewayParameterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GatewayParameterRepository::class)
 * @ORM\Table(name="gateway_parameters")
 * @ORM\HasLifecycleCallbacks
 */
class GatewayParameter
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRequired;

    /**
     * @ORM\ManyToOne(targetEntity=Gateway::class, inversedBy="gatewayParameters")
     * @ORM\JoinColumn(nullable=true)
     */
    private $gateway;

    /**
     * @ORM\ManyToOne(targetEntity=WebsiteGatewayConfiguration::class, inversedBy="gatewaysConfigurations",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $websiteGatewayConfiguration;

    /**
     * @ORM\ManyToOne(targetEntity=GatewayParameter::class, inversedBy="childGatewayParameter")
     */
    private $baseGatewayParameter;

    /**
     * @ORM\OneToMany(targetEntity=GatewayParameter::class, mappedBy="baseGatewayParameter")
     */
    private $childGatewayParameter;

    public function __construct()
    {
        $this->childGatewayParameter = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getIsRequired(): ?bool
    {
        return $this->isRequired;
    }

    public function setIsRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    public function getGateway(): ?Gateway
    {
        return $this->gateway;
    }

    public function setGateway(?Gateway $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getWebsiteGatewayConfiguration(): ?WebsiteGatewayConfiguration
    {
        return $this->websiteGatewayConfiguration;
    }

    public function setWebsiteGatewayConfiguration(?WebsiteGatewayConfiguration $websiteGatewayConfiguration): self
    {
        $this->websiteGatewayConfiguration = $websiteGatewayConfiguration;

        return $this;
    }

    public function getBaseGatewayParameter(): ?self
    {
        return $this->baseGatewayParameter;
    }

    public function setBaseGatewayParameter(?self $baseGatewayParameter): self
    {
        $this->baseGatewayParameter = $baseGatewayParameter;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildGatewayParameter(): Collection
    {
        return $this->childGatewayParameter;
    }

    public function addChildGatewayParameter(self $childGatewayParameter): self
    {
        if (!$this->childGatewayParameter->contains($childGatewayParameter)) {
            $this->childGatewayParameter[] = $childGatewayParameter;
            $childGatewayParameter->setBaseGatewayParameter($this);
        }

        return $this;
    }

    public function removeChildGatewayParameter(self $childGatewayParameter): self
    {
        if ($this->childGatewayParameter->contains($childGatewayParameter)) {
            $this->childGatewayParameter->removeElement($childGatewayParameter);
            // set the owning side to null (unless already changed)
            if ($childGatewayParameter->getBaseGatewayParameter() === $this) {
                $childGatewayParameter->setBaseGatewayParameter(null);
            }
        }

        return $this;
    }
}
