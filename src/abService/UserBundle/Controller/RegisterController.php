<?php

namespace App\abService\UserBundle\Controller;

use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\abService\ProjectBaseBundle\Services\MailerSender;
use App\abService\UserBundle\Form\UserChangePasswordFormType;
use App\abService\UserBundle\Form\UserResetPasswordFormType;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{

    /**
     * @Route("/register", name="user_register", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        return $this->render('register/index.html.twig', []);
    }
}
