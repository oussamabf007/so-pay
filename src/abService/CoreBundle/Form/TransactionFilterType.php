<?php


namespace  App\abService\CoreBundle\Form;

use App\abService\CoreBundle\Enum\TransactionEnumType;
use App\abService\CoreBundle\Services\TransactionHelper;
use App\Entity\Gateway;
use App\Entity\User;
use App\Entity\Website;
use App\Repository\GatewayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class TransactionFilterType extends AbstractType{

    private $security;
    private $transactionHelper;

    public function __construct(Security $security, TransactionHelper $transactionHelper)
    {
        $this->security= $security;
        $this->transactionHelper= $transactionHelper;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){

        //get current user.
        $currentUser = $this->security->getUser();
        $gateways = $this->transactionHelper->getGateways($currentUser);
        $websites = $currentUser->getWebsites();
        $builder
            ->add('gateways', EntityType::class,array(
                'required' => false,
                'label'=> 'Filter by gateways',
                'placeholder' => 'All gateways',
                'multiple' => true,
                'expanded' => false,
                'attr'=>array('class'=>"form-control select2"),
                'class' => 'App\Entity\Gateway',
                'choice_label' => function(Gateway $gateway){
                    return $gateway->getName();
                },
                'choice_value' => function(Gateway $gateway){
                    return $gateway->getToken();
                },
                'choices'=>$gateways
            ))
            ->add('sites', EntityType::class,array(
                'label'=> 'Filter by websites',
                'required' => false,
                'multiple' => true,
                'expanded' => false,
                'attr'=>array('class'=>"form-control select2"),
                'class' => 'App\Entity\Website',
                'choice_label' => function(Website $website){
                    return $website->getUrl();
                },
                'choice_value' => function(Website $website){
                    return $website->getToken();
                },
                'choices'=>$websites
            ))
            ->add('status', ChoiceType::class,array(
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'label'=> 'Filter by status',
                'attr'=>array('class'=>"form-control select2"),
                'choices'=>TransactionEnumType::getAllStatus(),
                'choice_label'=>function($status){
                    return TransactionEnumType::getStatusName($status);
                }
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}