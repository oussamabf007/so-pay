<?php


namespace  App\abService\CoreBundle\Form;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){


        $builder
            ->add('firstName', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('lastName', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('email', EmailType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('websites', EntityType::class,array(
                'required' => false,
                'placeholder' => 'Choose websites for this user',
                'multiple' => true,
                'attr'=>array('class'=>"form-control select2"),
                'class' => 'App\Entity\Website',
                'choice_label' => function($website){
                    return $website->getUrl();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isActivated LIKE :isActivated')
                        ->setParameter('isActivated', true);
                },

            ))
            ->add('isActivated', CheckboxType::class,array(
                'attr'=>array('class'=>"form-control"),
                'required'=>false

            ))
            ->add('isAdmin', CheckboxType::class,array(
                'attr'=>array('class'=>"form-control"),
                'required'=>false

            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}