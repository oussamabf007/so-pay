<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\CoreBundle\Controller;

use App\abService\ProjectBaseBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/support", name="support_")
 */
class SupportController extends AbstractController
{
    /**
     * @Route("/", name="support_index")
     */
    public function index(Request $request)
    {
        return $this->render('support/index.html.twig', []);
    }
}
