<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\CoreBundle\Controller;

use App\abService\CoreBundle\Form\WebsiteFormType;
use App\abService\GatewaysBundle\Form\WebsiteGateways;
use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\Entity\Gateway;
use App\Entity\GatewayParameter;
use App\Entity\WebsiteGatewayConfiguration;
use App\Repository\GatewayRepository;
use App\Repository\WebsiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Website;

/**
 * @Route("/websites", name="websites_")
 */
class WebsitesController extends AbstractController
{
    //website repository.
    private $websiteRepository;

    //EntityManager.
    private $entityManager;

    /**
     * ClientsController constructor.
     * @param WebsiteRepository $websiteRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(WebsiteRepository $websiteRepository, EntityManagerInterface $entityManager)
    {
        $this->websiteRepository = $websiteRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("websites/{id}", name="delete_website", methods={"DELETE","GET"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $website = $em->getRepository(Category::class)->find($id);

        if (!$website) {
            return $this->json([
                "msg" => "Website does not exist",
            ]);
        }
        $em->remove($website);
        $em->flush();
        return $this->json([
            "msg" => "Website deleted"
        ]);
    }



    /**
     * @Route("/", name="websites_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NoResultException
     */
    public function index(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            //Getrequested data.
            $data = $request->query->all();

            //Add control on user role to add user.id in condition
            //This control is made in controller for security reasons.
            if (!$this->isGranted("ROLE_ADMIN")) {
                $data['join'][] = array(
                    "join" => "t.users",
                    "alias" => 'user',
                    "condition" => "user.id = " . $this->getCurrentUser()->getId()
                );
            }

            //Get data.
            $ajaxDataResults = $this->websiteRepository->getAjaxDataTableData($data);

            //Set the data as wanted.
            $ajaxDataResults['data'] = $this->formatData($ajaxDataResults['data']);
            $ajaxDataResults['recordsFiltered'] = count($ajaxDataResults['data']);

            //return data.
            return new JsonResponse($ajaxDataResults);
        }
        //render http resposne.
        return $this->render('websites/index.html.twig', []);
    }

    /**
     * @Route("/new", name="add_website")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addWebsite(Request $request)
    {
        //create new website instance.
        $website = new website();

        //Create website form.
        $form = $this->createForm(WebsiteFormType::class, $website);

        //handle request.
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Save data to database.
                $this->entityManager->persist($website);
                $this->entityManager->flush();
                $this->addFlash('success', "Website was created successfully");

                //If user is not admin we set this website to his website listes.
                if (!$this->isGranted("ROLE_ADMIN")) {
                    $this->getCurrentUser()->addWebsite($website);
                    //Save data to database.
                    $this->entityManager->persist($this->getCurrentUser());
                    $this->entityManager->flush();

                    //Redirect user to the edit page.
                    return $this->redirectToRoute("websites_edit_website", array(
                        "token" => $website->getToken()
                    ));
                } else {
                    //Manage gateways.
                    return $this->redirectToRoute("websites_edit_website_gateways", array("token" => $website->getToken()));
                }
            } else {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }
            }
        }

        //Return http response.
        return $this->render('websites/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{token}", name="edit_website")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editWebsite(Request $request, $token)
    {
        //Get the requested user by his token.
        $website = $this->websiteRepository->findOneByToken($token);

        //If website not found throw error.
        if (is_null($website))
            throw $this->createNotFoundException('This website does not exist');

        if (!$this->isGranted("ROLE_ADMIN") && !$this->getCurrentUser()->getWebsites()->contains($website)) {
            $this->addFlash('error', "You are not allowed!");
            return $this->redirectToRoute("dashboard_dashboard_index");
        }

        //Create website edit form.
        $form = $this->createForm(WebsiteFormType::class, $website);

        //handle request.
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Save data to database.
                $this->entityManager->persist($website);
                $this->entityManager->flush();
                $this->addFlash('success', $website->getUrl() . " was updated successfully");

                //Manage gateways.
                if ($this->isGranted("ROLE_ADMIN")) {
                    return $this->redirectToRoute("websites_edit_website_gateways", array("token" => $token));
                }
            } else {
                $this->formErrorsFlashMessage($form);
            }
        }
        return $this->render('websites/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/(token)", name="delete_website")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deletewebsite($token)
    {
        $website = $this->websiteRepository->findOneByToken($token);

        if (is_null($website))
            throw $this->createNotFoundException('this website does not exist');

        if (!$this->isGranted("ROLE_ADMIN") && !$this->getCurrentUser()->getWebsites()->contains($website)) {
            $this->addFlash('error', "You are not allowed!");
        }


        $this->entityManager->remove($website);
        $this->entityManager->flush();
        $this->addFlash('success', "website removed successfully");

        /*    if ($this->isGranted("ROLE_ADMIN")) {
            return $this->redirectToRoute("websites", array("token" => $token));
        } */

        return $this->render('websites/index.html.twig');
    }

    /**
     * @Route("/edit/{token}/gateways", name="edit_website_gateways")
     * @param Request $request
     * @param $token
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function setGateways(Request $request, $token)
    {
        //Get requested website by token.
        $website = $this->websiteRepository->findOneByToken($token);

        //If website not found throw error.
        if (is_null($website))
            throw $this->createNotFoundException('This website does not exist');
        elseif (!$this->isGranted("ROLE_ADMIN")) {
            $this->addFlash('error', "You are not allowed!");
            return $this->redirectToRoute("dashboard_dashboard_index");
        }

        //Prepare gateways form.
        $form = $this->createForm(WebsiteGateways::class, $website);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //Save changes.
                $this->entityManager->persist($website);
                $this->entityManager->flush();

                //notify user that gateways configuration were updated successfully.
                $this->addFlash('success', $website->getUrl() . " gateway configurations were updated successfully");
            } else {
                $this->formErrorsFlashMessage($form);
            }
        } else {
            /**Gateways tools**/
            //GatewayRpositiory.
            $gatewayRepository = $this->getDoctrine()->getRepository(Gateway::class);
            //Get all gateways.
            $allGateways = $gatewayRepository->findAll();

            /**WebsiteGatewaysConfiguration tools**/
            //Check if this gateway already has a configuration for this website.
            $websiteGatewaysConfigurationRepository = $this->getDoctrine()->getRepository(WebsiteGatewayConfiguration::class);

            //Create all gateways configuration for this website then we will ddisable/Enable those configurations.
            foreach ($allGateways as $gateway) {
                //If this gateway already has a configuration for this website.
                $websiteGatewayConfiguration = $websiteGatewaysConfigurationRepository->findOneByGatewayAndWebsite($gateway, $website);
                //Create configurationParameters for each gateway if not exist.
                if (is_null($websiteGatewayConfiguration)) {

                    $websiteGatewayConfiguration = new WebsiteGatewayConfiguration();
                    $websiteGatewayConfiguration->setWebsite($website);
                    $websiteGatewayConfiguration->setGateway($gateway);
                }

                //Check all gatewayConfiguration parameters.
                $gatewayParameters = $gateway->getGatewayParameters();
                foreach ($gatewayParameters as $gatewayParameter) {
                    //if parameter is missing.
                    if (!$websiteGatewayConfiguration->getGatewaysConfigurationsBases()->contains($gatewayParameter)) {
                        //Create new parametere.
                        $websiteGatewayParameter = new GatewayParameter();
                        $websiteGatewayParameter->setIsRequired($gatewayParameter->getIsRequired());
                        $websiteGatewayParameter->setName($gatewayParameter->getName());
                        $websiteGatewayParameter->setValue($gatewayParameter->getValue());
                        $websiteGatewayParameter->setBaseGatewayParameter($gatewayParameter);

                        //Add gatewayparameter to websiteGatewayConfiguration.
                        $websiteGatewayConfiguration->addGatewaysConfiguration($websiteGatewayParameter);
                    }
                }

                //add gatewayConfiguration to website.
                $website->addGatewaysConfiguration($websiteGatewayConfiguration);
            }
            $this->entityManager->persist($website);
            $this->entityManager->flush();

            //Recreate form with website new data.
            $form = $this->createForm(WebsiteGateways::class, $website);
        }

        return $this->render('websites/website_gateways.html.twig', [
            'form' => $form->createView(),
            'website' => $website,
        ]);
    }

    /**
     * @param $data
     *
     * Set the data to correspond with the datatable format.
     * @return array
     */
    private function formatData($data)
    {

        $result = [];
        $dataRow = [];


        foreach ($data as $website) {
            $dataRow['t_name'] =  "<a href='" .
                $this->generateUrl('websites_edit_website', ['token' => $website['t_token']]) .
                "' title='edit'>" . $website['t_name'] . "</a>";
            $dataRow['t_url'] = $website['t_url'];
            $dataRow['t_ipAddress'] = is_null($website['t_ipAddress']) ? "-" : $website['t_ipAddress'];
            $dataRow['t_secretKey'] = $website['t_secretKey'];
            $dataRow['t_hashKey'] = $website['t_hashKey'];
            $dataRow['t_createdAt'] = $website['t_createdAt'];
            $dataRow['t_updatedAt'] = $website['t_updatedAt'];
            $dataRow['t_action'] = "<a href='" .
                $this->generateUrl('websites_edit_website', ['token' => $website['t_token']]) .
                "' title='edit'><i class='fa fa-edit fa-2x'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href='" .
                $this->generateUrl('websites_delete_website', ['token' => $website['t_token']]) .
                "' title='delete'><i class='fa fa-trash fa-2x'></i></a>";

            //Change the isActivated Value by text value
            if ($website['t_isActivated']) {
                $dataRow['t_isActivated'] = "<span class='badge badge-success'>Activated</span>";
            } else {
                $dataRow['t_isActivated'] = "<span class='badge badge-secondary'>Deactivated</span>";
            }

            $result[] = $dataRow;
        }

        return $result;
    }
}
