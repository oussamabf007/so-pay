<?php


namespace App\abService\GatewaysBundle\Form;

use App\Entity\GatewayParameter;
use App\Entity\WebsiteGatewayConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebsiteGatewayConfigurationFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("gatewaysConfigurations", CollectionType::class, array(
                'entry_type' => GatewayParameterFormType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => 'gateways parameters'
            ))
            ->add('isActivated', CheckboxType::class, array(
                'attr' => array('class' => "form-control"),
                'required' => false,
                'label' => "Activate Gateway."
            ));;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WebsiteGatewayConfiguration::class,
        ]);
    }
}