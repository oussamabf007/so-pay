<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 27/10/20
 * Time: 23:00
 */

namespace App\abService\PaymentBundle\Services;

use App\Entity\Transaction;
use App\Entity\WebsiteGatewayConfiguration;
use App\Repository\WebsiteGatewayConfigurationRepository;
use App\Repository\WebsiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TransactionManager implements TransactionManagerInterface
{

    //Container service
    private $locator;

    //Website repository.
    private $websiteRepository;

    //Entity Manager.
    private $em;

    //HttpClientInterface.
    private $client;

    private $requirments = array(
        "create_transaction" => array(
            "secret_identifier" => array(
                "type" => "is_string",
                "required" => true
            ),
            "return_url" => array(
                "type" => "is_string",
                "required" => true
            ),
            "amount" => array(
                "type" => "is_numeric",
                "required" => true
            ),
            "currency" => array(
                "type" => "is_string",
                "required" => false
            ),
            "order_id" => array(
                "type" => "is_string",
                "required" => true
            )
        ),
        "transaction_result" => array(
            "secret_identifier" => array(
                "type" => "is_string",
                "required" => true
            ),
            "transaction_token" => array(
                "type" => "is_string",
                "required" => true
            )
        )
    );

    /**
     * TransactionManager constructor.
     * @param WebsiteRepository $websiteRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(WebsiteRepository $websiteRepository, EntityManagerInterface $em, HttpClientInterface $client, ContainerInterface $locator)
    {
        $this->websiteRepository = $websiteRepository;
        $this->em = $em;
        $this->client = $client;
        $this->locator = $locator;
    }

    /**
     * Returns an array of service types required by such instances, optionally keyed by the service names used internally.
     *
     * For mandatory dependencies:
     *
     *  * ['logger' => 'Psr\Log\LoggerInterface'] means the objects use the "logger" name
     *    internally to fetch a service which must implement Psr\Log\LoggerInterface.
     *  * ['loggers' => 'Psr\Log\LoggerInterface[]'] means the objects use the "loggers" name
     *    internally to fetch an iterable of Psr\Log\LoggerInterface instances.
     *  * ['Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => 'Psr\Log\LoggerInterface']
     *
     * otherwise:
     *
     *  * ['logger' => '?Psr\Log\LoggerInterface'] denotes an optional dependency
     *  * ['loggers' => '?Psr\Log\LoggerInterface[]'] denotes an optional iterable dependency
     *  * ['?Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => '?Psr\Log\LoggerInterface']
     *
     * @return array The required service types, optionally keyed by service names
     */
    public static function getSubscribedServices()
    {
        return array(
            "edahab_service" => "App\abService\PaymentBundle\Services\EDahabService",
            "waafiPay_service" => "App\abService\PaymentBundle\Services\WaafiPayService"
        );
    }

    /**
     * check requested data are valid.
     *
     * @param $hash
     * @param $data
     * @return array
     */
    public function checkRequest($hash, $data, $actionName)
    {
        //First we check if all required parameters exists.
        $parametersCheck = $this->checkRequiredParameters($data, $actionName);

        if ($parametersCheck['status'] == "error") {
            return $parametersCheck;
        } else {

            $response = array(
                "status" => "error",
            );

            //Get website secret_key.
            $secretKey = $data['secret_identifier'];
            //Get website by secret key.
            $website = $this->websiteRepository->findOneBySecretKey($secretKey);

            return $secretKey;
            //Check if secret key is correct.
            if (is_null($website)) {
                $response["error"] = TransactionManager::WEBSITE_NOTE_FOUND_ERROR;
            } //Check if the website is activated.
            elseif (!$website->getIsActivated()) {
                $response['error'] = TransactionManager::WEBSITE_ACTIVATED_ERROR;
            } //Check if the website has activated gateways.
            elseif (count($this->websiteRepository->getActivatedGateways($website->getId())) == 0) {
                $response['error'] = TransactionManager::WEBSITE_HAS_NO_GATEWAY_ERROR;
            } //Check if hash is correct.
            elseif ($hash != hash(TransactionManager::HASH_ALGO, json_encode($data) . $website->getHashKey())) {
                $response["error"] = TransactionManager::SECURITY_ERROR . " " . hash(TransactionManager::HASH_ALGO, json_encode($data) . $website->getHashKey());
            } else {
                $response["status"] = "success";
                $response["website"] = $website;
            }
            return $response;
        }
    }

    /**
     * @param $parameters
     * @param $actionName
     * @return array
     */
    public function checkRequiredParameters($parameters, $actionName)
    {

        $response = array(
            "status" => "error",
            "error" => "UNKNOWN",
        );

        //Parcoure requirement to check if invalid type or missing param.
        foreach ($this->requirments[$actionName] as $constraintName => $constraint) {

            //If field is required we check if it's missing or not
            if ($constraint['required']) {

                if (!isset($parameters[$constraintName]) || $parameters[$constraintName] == "") {
                    $response['error'] = TransactionManager::MISSING_PARAMETERS . " $constraintName";
                    return $response;
                }
            }

            /**we check if the type is correct.*/
            //Get field type
            $fieldType = $constraint['type'];

            if (isset($parameters[$constraintName])) {
                if (!$fieldType($parameters[$constraintName])) {
                    //Return error.
                    $response['error'] = str_replace(
                        ['__PARAMETER__', '__TYPE__'],
                        [$constraintName, str_replace('is_', '', $constraint['type'])],
                        TransactionManager::INVALID_TYPE
                    );
                    return $response;
                }
            }
        }

        return array("status" => "success");
    }

    /**
     * @param $transactionData
     * @param $website
     * @return Transaction
     */
    public function createTransaction($transactionData, $website)
    {
        //Crete transaction.
        $transaction = new Transaction();
        $transaction->setWebsite($website);
        $transaction->setWebsiteOrderId($transactionData['order_id']);
        $transaction->setAmount($transactionData['amount']);
        $transaction->setReturnUrl($transactionData['return_url']);

        //Set the currency
        if (isset($transactionData['currency'])) {
            $transaction->setCurrency($transactionData['currency']);
        }

        if (isset($transactionData['item'])) {
            $transaction->setExtraData(($transactionData["item"]));
        }

        //Generate transaction order.
        $transaction->generateTransactionOrder();

        //Save Transaction to database.
        $this->em->persist($transaction);
        $this->em->flush();

        //Return transaction
        return $transaction;
    }

    /**
     * @param Transaction $transaction
     * @param $extraFields
     * @param string $returnUrl
     * @param string $callbackUrl
     * @return
     */
    public function handleTransaction($transaction, $extraFields, $returnUrl = "", $callbackUrl = "")
    {
        //Get gateway service.
        $gateway = $transaction->getGateway();
        $gatewayService = $this->locator->get($gateway->getServiceId());

        //get gatewayConfiguration.
        $websiteGatewaysConfigurationRepository = $this->em->getRepository(WebsiteGatewayConfiguration::class);

        $websiteGatewayConfigurations = $websiteGatewaysConfigurationRepository->findOneByGatewayAndWebsite($gateway, $transaction->getWebsite());
        $websiteGatewayConfigurations = $websiteGatewayConfigurations->getGatewaysConfigurations()->getValues();

        //Init configuration table.
        $configuration = array("gateway_param" => [], "transaction_data" => $extraFields);

        //Preparing configuration table. => set gateway parameters.
        foreach ($websiteGatewayConfigurations as $websiteGatewayConfiguration) {
            $configuration["gateway_param"][$websiteGatewayConfiguration->getName()] = $websiteGatewayConfiguration->getValue();
        }

        //Get selected Gateway response.
        $response = $gatewayService->execute($transaction, $configuration, $returnUrl, $callbackUrl);

        //Return response.
        return $response;
    }

    /**
     * @param $transaction
     * @param $request
     * @return
     */
    public function handleTransactionCallback($transaction, $request)
    {
        //Get gateway.
        $gateway = $transaction->getGateway();
        $gatewayService = $this->locator->get($gateway->getServiceId());

        //get gatewayConfiguration.
        $websiteGatewaysConfigurationRepository = $this->em->getRepository(WebsiteGatewayConfiguration::class);

        //Get gateway configuration.
        $websiteGatewayConfigurations = $websiteGatewaysConfigurationRepository->findOneByGatewayAndWebsite($gateway, $transaction->getWebsite());
        $websiteGatewayConfigurations = $websiteGatewayConfigurations->getGatewaysConfigurations()->getValues();

        //Preparing configuration table. => set gateway parameters.
        foreach ($websiteGatewayConfigurations as $websiteGatewayConfiguration) {
            $configuration["gateway_param"][$websiteGatewayConfiguration->getName()] = $websiteGatewayConfiguration->getValue();
        }

        //Get api callback response.
        $callbackResponse = $gatewayService->callback($transaction, $configuration, $request);

        //Notify the user about this transaction.
        $this->callWebsiteCallback($transaction);

        //Return response.
        return $callbackResponse;
    }

    /**
     * @param $transaction
     */
    private function callWebsiteCallback($transaction)
    {

        //Get the website and his callback URL.
        $website = $transaction->getWebsite();
        $websiteCallbackURL = $website->getCallbackUrl();

        $dataToSend = ['order_id' => $transaction->getWebsiteOrderId(), 'transaction_token' => $transaction->getTransactionOrder()];
        $dataHash = hash(TransactionManager::HASH_ALGO, json_encode($dataToSend) . $website->getHashKey());
        $dataToSend["hash"] = $dataHash;

        //Request the website callback.
        $response = $this->client->request(
            'POST',
            $websiteCallbackURL,
            ['json' => $dataToSend]

        );
    }
}
