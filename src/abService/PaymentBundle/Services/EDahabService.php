<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/11/20
 * Time: 23:00
 */

namespace App\abService\PaymentBundle\Services;

use App\abService\CoreBundle\Enum\TransactionEnumType;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EDahabService implements EDahabServiceInterface
{
    //Entity Manager.
    private $em;

    //Client..
    private $client;

    //Edahab endpoints.
    const CHECK_INVOICE_STATUS_ENDPOINT = "CheckInvoiceStatus";
    const ISSUE_INVOICE_ENDPOINT = "IssueInvoice";

    /**
     * EDahabService constructor.
     * @param EntityManagerInterface $em
     * @param HttpClientInterface $client
     */
    public function __construct(EntityManagerInterface $em,HttpClientInterface $client)
    {
        $this->em = $em;
        $this->client = $client;
    }

    /**
     * @param Transaction $transaction
     * @param array $configuration
     * @param Request $request
     * @return mixed|void
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function callback(Transaction $transaction, $configuration = [], Request $request)
    {
        //Get needed gateway parameters from all parameters.
        $gatewayParameters = $this->getGatewayParameters($configuration['gateway_param']);

        //Get issueInvoice parameters.
        $checkInvoice = $this->prepareCheckInvoiceStatusParameters($transaction, $gatewayParameters);

        //Consum eDahab api.
        $apiResponse = $this->procedeTransaction($checkInvoice['endpointUrl'], $checkInvoice['data'], $checkInvoice['hash']);

        //If api success.
        if ($apiResponse->getStatusCode() == 200 && $apiResponse->toArray()['StatusCode'] == "Success" && $apiResponse->toArray()['StatusCode'] == 0) {
            //Update transaction status.
            if($apiResponse->toArray()['InvoiceStatus'] == "Paid"){
                $transaction->setStatus(TransactionEnumType::STATUS_ACCEPTED);
                $this->em->persist($transaction);
            }
            $callbackResponse = [
                "status" => "success",
                "redirect_response" => true,
                "redirect_url" => $transaction->getReturnUrl()
            ];

        }else{
            $callbackResponse = [
                "status" => "error",
                "redirect_response" => true,
                "redirect_url" => $transaction->getReturnUrl(),
                "error message" => "an error occured whith the gateway"
            ];
        }

        return $callbackResponse;

    }

    /**
     * Function called when user choose his gateway.
     * This function is called to process the payment by the choosen gateway.
     *
     * @param Transaction $transaction
     * @param array $configuration
     *          Contains:
     *              -gateway_param: wiche are the gateway parameters with there values configured in the gateway form.
     *              -transaction_data: wiche are the transaction extra data configured in the gateway form.
     *
     * @param string $returnUrl
     * @param string $calbackUrl
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function execute(Transaction $transaction, $configuration = [], $returnUrl = "", $calbackUrl = "")
    {
        $transaction->setStatus(2);

        //Get needed gateway parameters from all parameters.
        $gatewayParameters = $this->getGatewayParameters($configuration['gateway_param']);

        //Get issueInvoice parameters.
        $issueInvoice = $this->prepareIssueInovice($transaction, $gatewayParameters, $configuration['transaction_data']['edahab_number'], $returnUrl);

        //Consum eDahab api.
        $apiResponse = $this->procedeTransaction($issueInvoice['endpointUrl'], $issueInvoice['data'], $issueInvoice['hash']);

        //Update order if api response is success.
        if ($apiResponse->getStatusCode() == 200 && $apiResponse->toArray()['StatusCode'] == "Success" && $apiResponse->toArray()['StatusCode'] == 0) {

            //Get eDahab invoiceOrder.
            $eDahabInvoiceId = $apiResponse->toArray()['InvoiceId'];

            //Update transaction extraFields.
            $gatewayExtraFields = $transaction->getGatewayExtraFields();
            $gatewayExtraFields["extra_fields"][] = $configuration["transaction_data"];
            $gatewayExtraFields["InvoiceId"] = $eDahabInvoiceId;

            //Update transaction extraFields.
            $transaction->setGatewayExtraFields($gatewayExtraFields);

            //Update transaction status to pending.
            $transaction->setStatus(TransactionEnumType::STATUS_PENDING);

            $executeResponse = [
                "status" => "success",
                "redirect_response" => true,
                "redirect_url" => $gatewayParameters['redirect_URL'] . "?invoiceId=$eDahabInvoiceId"
            ];

        } else {
            $executeResponse = [
                "status" => "error",
                "redirect_response" => true,
                "redirect_url" => $transaction->getReturnUrl(),
                "error message" => "an error occured whith the gateway"
            ];
        }

        //save the modification made on the transaction.
        $this->em->persist($transaction);
        $this->em->flush();

        //Return the execution result.
        return $executeResponse;
    }

    /**
     * Get gateway parameters.
     *
     * @param $parameters
     * @return array
     */
    protected function getGatewayParameters($parameters)
    {
        return array(
            "APIKey" => $parameters['API key'],
            "agentCode" => $parameters['agent code'],
            "secretAPIkey" => $parameters['secret api key'],
            "endpoint_URL" => $parameters['Endpoint URL'],
            "redirect_URL" => $parameters['redirect url'],
        );
    }

    /**
     * @param Transaction $transaction
     * @param $gatewayParameters
     * @param $eDahabNumber
     * @return array  : endpointUrl, hash and the data.
     */
    protected function prepareIssueInovice(Transaction $transaction, $gatewayParameters, $eDahabNumber, $returnUrl)
    {

        //Prepare the return result.
        $result = array("endpointUrl" => $gatewayParameters["endpoint_URL"], "data" => array(), "hash" => "");

        //Prepare the data to send to the api.
        $data = array(
            "apiKey" => $gatewayParameters["APIKey"],
            "EdahabNumber" => $eDahabNumber,
            "Amount" => $transaction->getAmount(),
            "AgentCode" => $gatewayParameters["agentCode"],
            "ReturnUrl" => $returnUrl
        );
        $result["data"] = $data;

        //Prepare the hash.

        $result['hash'] = $this->getHash(json_encode($data) . $gatewayParameters['secretAPIkey']);

        return $result;
    }

    /**
     * Generate needed data to request CheckInvoiceStatus api.
     *
     * @param Transaction $transaction
     * @param $gatewayParameters
     * @return array
     */
    public function prepareCheckInvoiceStatusParameters(Transaction $transaction, $gatewayParameters){

        $result = array("endpointUrl" => $gatewayParameters["endpoint_URL"], "data" => array(), "hash" => "");
        //Prepare the data to send to the api.
        $data = array(
            "apiKey" => $gatewayParameters["APIKey"],
            "invoiceId" => ($transaction->getGatewayExtraFields())["InvoiceId"]
        );
        $result["data"] = $data;

        //Prepare the hash.

        $result['hash'] = $this->getHash(json_encode($data) . $gatewayParameters['secretAPIkey']);

        return $result;
    }

    /**
     * @param $endPointUrl
     * @param $data
     * @param $hash
     * @return \Symfony\Contracts\HttpClient\ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function procedeTransaction($endPointUrl, $data, $hash)
    {
        //Get return Url.
        $url = $endPointUrl . EDahabService::ISSUE_INVOICE_ENDPOINT . "?hash=$hash";

        $response = $this->client->request(
            'POST',
            $url,
            ['json' => $data]

        );

        //Return api response
        return $response;
    }

    /**
     * @param $data
     * @return string : hash of the data using SHA-256 algorithm.
     */
    protected function getHash($data)
    {

        return hash("sha256", $data);
    }

}