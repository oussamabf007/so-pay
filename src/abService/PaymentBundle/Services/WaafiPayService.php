<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/11/20
 * Time: 23:00
 */

namespace App\abService\PaymentBundle\Services;

use App\abService\CoreBundle\Enum\TransactionEnumType;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WaafiPayService implements WaafiPayServiceInterface
{
    //Entity Manager.
    private $em;

    //Client..
    private $client;

    //Edahab endpoints.
    const CHECK_INVOICE_STATUS_ENDPOINT = "CheckInvoiceStatus";
    const ISSUE_INVOICE_ENDPOINT = "IssueInvoice";

    /**
     * EDahabService constructor.
     * @param EntityManagerInterface $em
     * @param HttpClientInterface $client
     */
    public function __construct(EntityManagerInterface $em, HttpClientInterface $client)
    {
        $this->em = $em;
        $this->client = $client;
    }


    /**
     * Function called when user choose his gateway.
     * This function is called to process the payment by the choosen gateway.
     *
     * @param Transaction $transaction
     * @param array $configuration
     *          Contains:
     *              -gateway_param: wiche are the gateway parameters with there values configured in the gateway form.
     *              -transaction_data: wiche are the transaction extra data configured in the gateway form.
     *
     * @param string $returnUrl
     * @param string $callbackUrl
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function execute(Transaction $transaction, $configuration = [], $returnUrl = "", $callbackUrl = "")
    {
        $transaction->setStatus(2);

        //Get gateway parameters from all parameters.
        $gatewayParameters = $this->getGatewayParameters($configuration['gateway_param']);

        //Get parametersto request the paiement.
        $initiateTransactionParameters = $this->getInitiateTransactionParameter($gatewayParameters, "HPP_PURCHASE", $transaction, $returnUrl);

        //Consume WaafiAPI
        $apiResponse = $this->procedeTransaction($gatewayParameters['endpointUrl'], $initiateTransactionParameters);

        //IF response is a success.
        if ($apiResponse->getStatusCode() == 200 && $apiResponse->toArray()['errorCode'] == "0" && $apiResponse->toArray()['responseMsg'] == "RCS_SUCCESS") {
            //Change transaction status to pending.
            $transaction->setStatus(TransactionEnumType::STATUS_PENDING);
            //Get transaction extra field.
            $transactionExtraData = $transaction->getExtraData();

            //Add the api result to the transaction
            $transactionExtraData['gatewayResponse'] = $apiResponse->toArray();

            //Template success params.
            $successParams = $apiResponse->toArray()["params"];
            $successParams["transaction"] = $transaction;

            //Prepare the execute response.
            $executeResponse = [
                "status" => "success",
                "redirect_response" => false,
                "render_view" => true,
                "render_view_template" => "methods/wafii.html.twig",
                "render_view_parameters" => $successParams
            ];
        } else {

            //Set the transaction status to refused.
            $transaction->setStatus(TransactionEnumType::STATUS_REFUSED);
            //Get transaction extra field.
            $transactionExtraData = $transaction->getExtraData();

            //Add the api result to the transaction$transactionExtraData['gatewayResponse'] = $apiResponse->toArray();
            $transactionExtraData['gatewayResponseError'] = $apiResponse->toArray()["responseMsg"];
            $transaction->setExtraData($transactionExtraData);

            $executeResponse = [
                "status" => "error",
                "redirect_response" => true,
                "redirect_url" => $transaction->getReturnUrl(),
                "error message" => "an error occured whith the gateway"
            ];

        }

        //save the modification made on the transaction.
        $this->em->persist($transaction);
        $this->em->flush();

        //Return the execution result.
        return $executeResponse;
    }

    /**
     * @param $endPointUrl
     * @param $data
     * @param $hash
     * @return \Symfony\Contracts\HttpClient\ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function procedeTransaction($endPointUrl, $data)
    {
        //Get return Url.
        $url = $endPointUrl;

        $response = $this->client->request(
            'POST',
            $url,
            ['json' => $data]

        );

        //Return api response
        return $response;
    }

    /**
     * @param $gatewayParameters
     * @param $serviceName
     * @param $transaction
     * @param string $callbackUrl
     * @return array
     */
    private function getInitiateTransactionParameter($gatewayParameters, $serviceName, $transaction, $callbackUrl="")
    {
        $intiateTransactionParameters = [
            "schemaVersion" => "1.0",
            "requestId" => $transaction->getTransactionOrder(),
            "timestamp" => (new \DateTime())->getTimestamp(),
            "channelName" => "WEB",
            "serviceName" => $serviceName,
        ];

        if($serviceName == "HPP_PURCHASE"){
            $intiateTransactionParameters["serviceParams"] = array(
                "storeId" => $gatewayParameters["apiUserId"],
                "merchantUid" => $gatewayParameters["merchantUid"],
                "hppSuccessCallbackUrl" => $callbackUrl,
                "hppFailureCallbackUrl" => $callbackUrl,
                "hppKey" => $gatewayParameters["hppKey"],
                "paymentMethod" => "MWALLET_ACCOUNT",
                "hppRespDataFormat" => 4,
            );

            $intiateTransactionParameters["serviceParams"]["transactionInfo"] = array(
                "referenceId" => $transaction->getTransactionOrder(),
                "invoiceId" => $transaction->getWebsiteOrderId(),
                "amount" => $transaction->getAmount(),
                "currency" => "USD",
                "description" => ""
            );
        }elseif($serviceName == "HPP_GETRESULTINFO"){
            $intiateTransactionParameters["serviceParams"] = array(
                "storeId" => $gatewayParameters["apiUserId"],
                "merchantUid" => $gatewayParameters["merchantUid"],
                "hppKey" => $gatewayParameters["hppKey"]
            );
        }


        return $intiateTransactionParameters;
    }

    /**
     * @param $parameters
     * @return array
     */
    private function getGatewayParameters($parameters)
    {
        return array(
            "endpointUrl" => $parameters["Endpoint"],
            "merchantUid" => $parameters["merchantUid"],
            "apiUserId" => $parameters["apiUserId"],
            "hppKey" => $parameters["HPP_Key"]
        );
    }


    /**
     * @param Transaction $transaction
     * @param array $configuration
     * @param Request $request
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function callback(Transaction $transaction, $configuration = [], Request $request)
    {
        //get wafii token.
        $hppResultToken = $request->query->get("hppResultToken");

        //Get gateway parameters from all parameters.
        $gatewayParameters = $this->getGatewayParameters($configuration['gateway_param']);

        //Get request parameters.
        $initiateTransactionParameters = $this->getInitiateTransactionParameter($gatewayParameters, "HPP_GETRESULTINFO", $transaction);
        $initiateTransactionParameters["serviceParams"]["hppResultToken"] = $hppResultToken;

        //Consume WaafiAPI
        $apiResponse = $this->procedeTransaction($gatewayParameters['endpointUrl'], $initiateTransactionParameters);

        //IF response is a success.
        if ($apiResponse->getStatusCode() == 200 && $apiResponse->toArray()['errorCode'] == "0"
            && $apiResponse->toArray()['responseMsg'] == "RCS_SUCCESS") {
            //Change transaction status.
            switch ($apiResponse->toArray()["params"]["state"]){
                case "DECLINED":
                    $transaction->setStatus(TransactionEnumType::STATUS_REFUSED);
                    break;
                case "APPROVED":
                    $transaction->setStatus(TransactionEnumType::STATUS_ACCEPTED);
                    break;
            }

            //Get transaction extra field.
            $transactionExtraData = $transaction->getExtraData();

            //Add the api result to the transaction
            $transactionExtraData['gateway_result_response'] = $apiResponse->toArray();
            $transaction->setExtraData($transactionExtraData);

            $callbackResponse = [
                "status" => "success",
                "redirect_response" => true,
                "redirect_url" => $transaction->getReturnUrl()
            ];

            $this->em->persist($transaction);
            $this->em->flush();
        } else {
            $callbackResponse = [
                "status" => "error",
                "redirect_response" => true,
                "redirect_url" => $transaction->getReturnUrl(),
                "error message" => "an error occured whith the gateway"
            ];
        }

        return $callbackResponse;
    }
}
