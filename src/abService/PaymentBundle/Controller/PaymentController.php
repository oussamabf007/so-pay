<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\PaymentBundle\Controller;

use App\abService\CoreBundle\Enum\TransactionEnumType;
use App\abService\PaymentBundle\Form\GatewaysFormType;
use App\abService\PaymentBundle\Services\TransactionManager;
use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/payment", name="payment_")
 */
class PaymentController extends AbstractController
{
    //user repository.
    private $transactionRepository;

    //EntityManager.
    private $entityManager;

    //Client.
    private $client;

    /**
     * ClientsController constructor.
     * @param TransactionRepository $transactionRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TransactionRepository $transactionRepository, EntityManagerInterface $entityManager, HttpClientInterface $client)
    {
        $this->transactionRepository = $transactionRepository;
        $this->entityManager = $entityManager;
        $this->client = $client;
    }

    /**
     * payment page, to let the customer choose his payment gateway.
     *
     * @Route("/pay/{transaction_order}", name="payment_pay")
     * @param TransactionManager $transactionManager
     * @param Request $request
     * @param $transaction_order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pay(TransactionManager $transactionManager, Request $request, $transaction_order)
    {
        //Get requested transaction.
        $transaction = $this->transactionRepository->findOneByTransactionOrder($transaction_order);
        if (is_null($transaction))
            throw $this->createNotFoundException('Transaction not found');

        //Accept only created transactions.
        if (!in_array($transaction->getStatus(), [TransactionEnumType::STATUS_CREATED, TransactionEnumType::STATUS_PENDING]))
            die("cannot be paied");

        //Create gateways form
        $form = $this->createForm(
            GatewaysFormType::class,
            array(
                'website' => $transaction->getWebsite(),
                'em' => $this->entityManager,
            )
        );

        //Handle request.
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $gateway = $form->getData()['gateway'];

                //Get extraFields.
                $extraFields = $request->request->get("transaction_method_gateway");
                $extraFields = $extraFields['extraFields'];

                //Create redirectURL.
                $redirectUrl = $this->generateUrl("payment_payment_callback", array("transaction_order" => $transaction->getTransactionOrder()), 0);

                //Set the gateway for transaction.
                $transaction->setGateway($gateway);
                $this->entityManager->persist($transaction);
                

                //Get gateway response.
                $response = $transactionManager->handleTransaction($transaction, $extraFields, $redirectUrl);
                //Redirect user.
                if ($response['redirect_response'])
                    return $this->redirect($response['redirect_url']);
                elseif ($response['render_view']) {
                    return $this->render($response['render_view_template'], $response['render_view_parameters']);
                }
            } else {
                $this->formErrorsFlashMessage($form);
            }
        }

        //render pay view.
        return $this->render('payment/pay.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/result/{transaction_order}", name="payment_callback")
     *
     * @param TransactionManager $transactionManager
     * @param Request $request
     * @param $transaction_order
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function returnFromPayment(TransactionManager $transactionManager, Request $request, $transaction_order)
    {
        //Get requested transaction.
        $transaction = $this->transactionRepository->findOneByTransactionOrder($transaction_order);
        if (is_null($transaction))
            throw $this->createNotFoundException('Transaction not found');

        $response = $transactionManager->handleTransactionCallback($transaction, $request);

        //@TODO: add multiple response type for callbacks like cick topay.
        //Redirect user.
        if ($response['redirect_response'])
            return $this->redirect($response['redirect_url']);
    }

    /**
     *
     * Create transaction for specific client and return json result with the transaction_token.
     *
     * @Route("/api/create_transaction/{hash}", name="create_transaction",methods={"POST"})
     * @param TransactionManager $transactionManager
     * @param Request $request
     * @param $hash
     * @return JsonResponse : Contain the request result and the transaction_token.
     */
    public function createTransaction(TransactionManager $transactionManager, Request $request, $hash)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());

            //Check the request.
            $result = $transactionManager->checkRequest($hash, $data, "create_transaction");

            return $this->json([$result]);
            if ($result["status"] == "success") {
                //Get the requested website.
                $website = $result['website'];

                //Create transaction and give return the transaction token so the user will redirect to it.
                $transaction = $transactionManager->createTransaction($data, $website);
                //Add the transaction order (transaction_token) to the response
                $result["transaction_token"] = $transaction->getTransactionOrder();
                unset($result['website']);
            }
        } else {
            $result = array(
                "status" => "error",
                "error" => "Content type must be application/json"
            );
        }

        //Create json response.
        return new JsonResponse($result, 200, ['Content-Type: application/json']);
    }

    /**
     * Get the transaction result. Used to let wesites to get the result of the transaction status.
     *
     * @Route("/api/transaction_result/{hash}", name="transaction_result",methods={"POST"})
     * @param TransactionManager $transactionManager
     * @param Request $request
     * @param $hash
     * @return JsonResponse
     */
    public function transactionResult(TransactionManager $transactionManager, Request $request, $hash)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());

            //Check the request.
            $result = $transactionManager->checkRequest($hash, $data, "transaction_result");

            if ($result["status"] == "success") {
                //Get the requested website.
                $website = $result['website'];
                unset($result['website']);

                //Get the requested transaction.
                $transactionOrder = $data['transaction_token'];
                $transaction = $this->entityManager->getRepository(Transaction::class)
                    ->findOneByTransactionOrder($transactionOrder);

                //Add the transaction order (transaction_token) to the response
                $result["transaction_status"] = strtolower(TransactionEnumType::getStatusName($transaction->getStatus()));
            }
        } else {
            $result = array(
                "status" => "error",
                "error" => "Content type must be application/json"
            );
        }

        //Create json response.
        return new JsonResponse($result, 200, ['Content-Type: application/json']);
    }
}
