<?php

namespace  App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\abService\ProjectBaseBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    // ...

    /**
     * Load the site definition and redirect to the default page.
     *
     * @Route("/", name="default_route")
     */
    public function indexAction()
    {
        if($this->isGranted("IS_AUTHENTICATED_FULLY"))
            return $this->redirectToRoute('dashboard_dashboard_index');
        else{
            return $this->redirectToRoute('user_login');
        }
    }
}