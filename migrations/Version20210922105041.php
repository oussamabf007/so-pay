<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210922105041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gateway_parameters DROP FOREIGN KEY FK_4283D9322CA7288');
        $this->addSql('ALTER TABLE gateway_parameters ADD CONSTRAINT FK_4283D9322CA7288 FOREIGN KEY (base_gateway_parameter_id) REFERENCES gateway_parameters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4C18F45C82');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4C577F8E00');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4C18F45C82 FOREIGN KEY (website_id) REFERENCES websites (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4C577F8E00 FOREIGN KEY (gateway_id) REFERENCES gateways (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gateway_parameters DROP FOREIGN KEY FK_4283D9322CA7288');
        $this->addSql('ALTER TABLE gateway_parameters ADD CONSTRAINT FK_4283D9322CA7288 FOREIGN KEY (base_gateway_parameter_id) REFERENCES gateway_parameters (id)');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4C577F8E00');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4C18F45C82');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4C577F8E00 FOREIGN KEY (gateway_id) REFERENCES gateways (id)');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4C18F45C82 FOREIGN KEY (website_id) REFERENCES websites (id)');
    }
}
